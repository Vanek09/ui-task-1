package com.example.uitask1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.uitask1.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater).also { setContentView(it.root) }

        binding.customView.setOnClickListener(object : OnClick{
            override fun onClickRadioButton(title: String) {
                binding.selected = title
            }
        })
    }
}