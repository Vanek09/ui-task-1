package com.example.uitask1

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RadioButton
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.uitask1.databinding.CustomViewBinding
import java.lang.Exception
import kotlin.properties.Delegates

class CustomView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val binding = CustomViewBinding.inflate(LayoutInflater.from(context), this, true)

    private lateinit var listener: OnClick
    private lateinit var textButton: String
    private var backgroundButton by Delegates.notNull<Int>()

    init {
        val arr = context.obtainStyledAttributes(attrs, R.styleable.CustomView, 0,0)
        try {
            textButton = arr.getString(R.styleable.CustomView_textButton) ?: resources.getString(R.string.who_is_active)
            backgroundButton = arr.getColor(R.styleable.CustomView_backgroundButton, resources.getColor(R.color.black, null))
            setButton(textButton, backgroundButton)
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            arr.recycle()
        }
        showView()
    }

    private fun showView() {
        binding.apply {
            logicOfRadioGroup(topRadioButton, rightRadioButton, bottomRadioButton, leftRadioButton)
            buttonActive.setOnClickListener { onClick() }
        }
    }

    private fun logicOfRadioGroup(vararg radioButtons: RadioButton) {
        radioButtons.forEach { selectedRadioButton ->
            selectedRadioButton.setOnCheckedChangeListener{ _, isChecked ->
                if (isChecked) {
                    radioButtons
                        .filter { unselectedButton -> unselectedButton != selectedRadioButton }
                        .forEach { it.isChecked = false }

                    setButton(selectedRadioButton.text as String, selectedRadioButton.currentTextColor)
                    listener.onClickRadioButton(selectedRadioButton.text as String)
                }
            }
        }
    }

    private fun setButton(text: String, color: Int) {
        binding.apply {
            buttonActive.text = text
            buttonActive.backgroundTintList = ColorStateList.valueOf(color)
        }
    }

    private  fun onClick() {
        binding.apply {
            setButton(textButton, backgroundButton)
            listener.onClickRadioButton(resources.getString(R.string.no_active))
            rightRadioButton.isChecked = false
            leftRadioButton.isChecked = false
            topRadioButton.isChecked = false
            bottomRadioButton.isChecked = false
        }
    }

    fun setOnClickListener(listener: OnClick) {
        this.listener = listener
        listener.onClickRadioButton(resources.getString(R.string.no_active))
    }
}